from flask import Flask, request

app = Flask('Recruto_greeter')


@app.route('/')
def hello_recruito():
    name = request.args.get('name')
    message = request.args.get('message')
    return f"Hello {name}! {message}"


if __name__ == '__main__':
    app.run(debug=True, host='0.0.0.0', port=80)
